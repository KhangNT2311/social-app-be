import { Request, Response } from "express";
import { AuthUser, ErrorCodes, ErrorMessages, UserRole } from "../constants";
import { uploadToCloudinary, deleteFromCloudinary } from "../utils/cloudinary";
import {
  getPostsByChannelId,
  getPostsByAuthorId,
  getPostById,
  createPost,
  deletePost,
  getFollowedPosts,
  postById,
  updatePost,
  pinPost,
} from "../db";

const PostController = {
  postsByFollowing: async (req: Request, res: Response): Promise<any> => {
    const authUser = req.user as AuthUser;
    const { offset, limit } = req.query;
    const posts = await getFollowedPosts(authUser?._id, +offset, +limit);
    return res.send(posts);
  },
  postsByChannelId: async (req: Request, res: Response): Promise<any> => {
    const { channelId } = req.params;
    const { offset, limit } = req.query;
    const posts = await getPostsByChannelId(channelId, +offset, +limit);
    return res.send(posts);
  },
  postsByAuthorId: async (req: Request, res: Response): Promise<any> => {
    const { authorId } = req.params;
    const { offset, limit } = req.query;
    const posts = await getPostsByAuthorId(authorId, +offset, +limit);
    return res.send(posts);
  },
  postById: async (req: Request, res: Response): Promise<any> => {
    const { id } = req.params;
    const post = await getPostById(id);
    return res.send(post);
  },
  create: async (req: Request, res: Response): Promise<any> => {
    const authUser = req.user as AuthUser;
    const { title } = req.body;
    const image = req.file;

    if (!title && !image) {
      return res.status(400).send("Post title or image is required.");
    }
    if (image && !image.mimetype.match(/image-*/)) {
      return res.status(ErrorCodes.Bad_Request).send("Please upload an image.");
    }

    let imageUrl: string;
    let imagePublicId: string;

    try {
      if (image) {
        const uploadImage = await uploadToCloudinary(image, "post");
        if (!uploadImage.secure_url) {
          return res.status(ErrorCodes.Internal).send(ErrorMessages.Generic);
        }
        imageUrl = uploadImage.secure_url;
        imagePublicId = uploadImage.public_id;
      }
      const newPost: any = await createPost(
        title,
        imageUrl,
        imagePublicId,
        authUser._id
      );
      return res.send(newPost);
    } catch (error) {
      res.status(ErrorCodes.Internal).send("Can not create post");
    }
  },
  update: async (req: Request, res: Response): Promise<any> => {
    const authUser = req.user as AuthUser;
    const { postId, title, imageToDeletePublicId } = req.body;
    const image = req.file;

    if (authUser.role !== UserRole.SuperAdmin) {
      const post: any = await postById(postId);
      if (post.author.toString() !== authUser._id.toString()) {
        return res.status(ErrorCodes.Bad_Request).send("Unauthorized");
      }
    }

    if (imageToDeletePublicId) {
      const deleteImage = await deleteFromCloudinary(imageToDeletePublicId);
      if (deleteImage.result !== "ok") {
        return res.status(ErrorCodes.Internal).send(ErrorMessages.Generic);
      }
    }

    let imageUrl: string;
    let imagePublicId: string;
    if (image) {
      const uploadImage = await uploadToCloudinary(image, "post");
      if (!uploadImage.secure_url) {
        return res.status(ErrorCodes.Internal).send(ErrorMessages.Generic);
      }
      imageUrl = uploadImage.secure_url;
      imagePublicId = uploadImage.public_id;
    }

    const updatedPost = await updatePost(
      postId,
      title,
      imageUrl,
      imagePublicId,
      imageToDeletePublicId
    );
    return res.send(updatedPost);
  },
  delete: async (req: Request, res: Response): Promise<any> => {
    const { id, imagePublicId } = req.body;
    const authUser = req.user as AuthUser;

    if (authUser.role !== UserRole.SuperAdmin) {
      const post: any = await postById(id);
      if (post.author.toString() !== authUser._id.toString()) {
        return res.status(ErrorCodes.Bad_Request).send(ErrorMessages.Generic);
      }
    }

    if (imagePublicId) {
      const deleteImage = await deleteFromCloudinary(imagePublicId);
      if (deleteImage.result !== "ok") {
        return res.status(ErrorCodes.Internal).send(ErrorMessages.Generic);
      }
    }

    const deletedPost = await deletePost(id);
    return res.send(deletedPost);
  },
  pin: async (req: Request, res: Response): Promise<any> => {
    const { id, pinned } = req.body;
    const updatedPost = await pinPost(id, pinned);
    return res.send(updatedPost);
  },
};

export default PostController;
